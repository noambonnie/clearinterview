"use strict";

const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const Organization = require("./api/v1/organization");
const config = require("config");

const app = express();

// Simple middleware to log requests.
const requestLogger = (req, res, next) => {
    console.info(`[${Date()}] ${req.method} ${req.originalUrl}`);
    next();
};

async function startServer(port) {

    try {
        // Both these options are needed to suppress some Node deprecation messages.
        //
        // useNewUrlParser - Use Mongo Driver's new URL parsing
        // useUnifiedTopology - Set to true to opt in to using the MongoDB driver's new connection management engine. 
        //                      You should set this option to true, except for the unlikely case that it prevents you from 
        //                      maintaining a stable connection
        console.info("Starting server...");
        // For Heroku I get the DB connection string from an env var. Otherwise take it from the config.
        await mongoose.connect(process.env.MONGODB_URI || config.get("dbURL"), { useNewUrlParser: true, useUnifiedTopology: true });

        console.info("Connected to DB");
    
        // Setup express
        app.use(requestLogger);
        app.use(bodyParser.json());
    
        app.get("/v1/organization/search", Organization.v1SearchOrganization);
        app.get("/v1/organization/:id", Organization.v1GetOrganization);

        app.post("/v1/organization", Organization.v1AddOrganization);
            
        app.listen(port, () => {
            console.info(`Listening on port ${port}`);
        }).on("error", (err) => {
            console.error("Express error", err);
        });
    } catch(err) {
        console.error(`failed to start server: ${err}`);
    }
}

// For heroku I take the port from an env var. Otherwise from config.
startServer(process.env.PORT || config.get("listenPort"));