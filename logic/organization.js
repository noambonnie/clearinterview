const mongoose = require("mongoose");
const Organization = require("../db/organization");

const formatOrgResp = (org) => {
    // Format the result
    const result = {
        id: org._id,
        name: org.name,
        employeesNumber: org.employeesNumber,
        isPublic: org.isPublic,
        inceptionDate: org.inceptionDate.getTime()
    };

    return result;
};

module.exports.getOrganization = async(id) => {
    try {

        // Treat invalid IDs as not found.
        if (!mongoose.Types.ObjectId.isValid(id)) {
            return null;
        }

        const org = await Organization.findById(id).exec();
        
        if (org == null) {
            return null;
        }

        return formatOrgResp(org);
    } catch (err) {
        console.error(`failed to read organization ${id} from db: ${err}`);
        throw(err);
    }
};

module.exports.addOrganization = async(organization) => {
    // At this point we expect a valid organization object after the API level did the validations.
    const newOrg = new Organization({
        name: organization.name, 
        inceptionDate: organization.inceptionDate, 
        employeesNumber: organization.employeesNumber,
        isPublic: organization.isPublic,
    });

    try {
        const org = await newOrg.save();
        return formatOrgResp(org);
    } catch (err) {
        console.error(`failed to write organization to db: ${err}`);
        throw(err);
    }
};

module.exports.searchOrganization = async(criteria) => {

    try {
        let limit = 100;

        // Some sanity on the limit.
        if (criteria.limit != null && criteria.limit < 1000 && criteria.limit > 0) {
            limit = criteria.limit;
        }

        let offset = 0;
        if (criteria.offset != null) {
            offset = criteria.offset;
        }

        // Build the query
        let query = {};
        if (criteria.inceptionFrom != null || criteria.inceptionTo != null) {
            query.inceptionDate = {};
            if (criteria.inceptionFrom != null) {
                query.inceptionDate.$gte = criteria.inceptionFrom;
            }

            if (criteria.inceptionTo != null) {
                query.inceptionDate.$lte = criteria.inceptionTo;
            }
        }

        if (criteria.employeesFrom != null || criteria.employeesTo != null) {
            query.employeesNumber = {};
            if (criteria.employeesFrom != null) {
                query.employeesNumber.$gte = criteria.employeesFrom;
            }

            if (criteria.employeesTo != null) {
                query.employeesNumber.$lte = criteria.employeesTo;
            }
        }

        if (criteria.isPublic != null) {
            query.isPublic = criteria.isPublic;
        }

        if (criteria.name) {
            query.name = new RegExp(criteria.name, "i");
        }

        const orgs = await Organization.find(query).limit(limit).skip(offset).exec();
        return { results: orgs.map((org) => formatOrgResp(org)) };
    } catch (err) {
        console.error(`failed to search organization: ${err}`);
        throw(err);
    }
};
