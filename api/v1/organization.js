const Organization = require("../../logic/organization");

module.exports.v1GetOrganization = async(req, res) => {
    if (!req.params.id) {
        res.status(400).json({"error": "invalid input"});
        return;
    }

    try {
        const org = await Organization.getOrganization(req.params.id);
        if (org == null) {
            res.status(204).end(); // There's debate whether 404 or 204 or 200 with empty body should be returned. I find 204 to be the most appropriate.
            return;
        }
        res.json(org);
    } catch(err) {
        console.error(`v1GetOrganization: failed to get organization ${req.params.id}: ${err}`);
        res.status(500).json({ error: "failed to get organization" });
    }
};

module.exports.v1SearchOrganization = async(req, res) => {
    if (req.query.inceptionFrom && isNaN(req.query.inceptionFrom) || 
        req.query.inceptionTo && isNaN(req.query.inceptionTo) || 
        req.query.employeesFrom && isNaN(req.query.employeesFrom) || 
        req.query.employeesTo && isNaN(req.query.employeesTo) || 
        req.query.offset && isNaN(req.query.offset) ||
        req.query.limit && isNaN(req.query.limit) ||
        req.query.isPublic && !["true", "false"].includes(req.query.isPublic)) {
        res.status(400).json({"error": "invalid input"});
        return;
    }

    try {
        const searchCriteria = {
            inceptionFrom: req.query.inceptionFrom != null && Number(req.query.inceptionFrom) || null,
            inceptionTo: req.query.inceptionTo != null && Number(req.query.inceptionTo) || null,
            employeesFrom: req.query.employeesFrom != null && Number(req.query.employeesFrom) || null,
            employeesTo: req.query.employeesTo != null && Number(req.query.employeesTo) || null,
            name: req.query.name,
            isPublic: req.query.isPublic != null && req.query.isPublic === "true" || null,
            offset: req.query.offset != null && Number(req.query.offset) || null,
            limit: req.query.limit != null && Number(req.query.limit) || null,
        };

        const orgs = await Organization.searchOrganization(searchCriteria);
        res.json(orgs);
    } catch(err) {
        console.error(`v1GetOrganization: failed to search organization: ${err}`);
        res.status(500).json({ error: "failed to search organization" });
    }
};

module.exports.v1AddOrganization = async(req, res) => {
    // Whatever other validations we want. Assuming this for now... 
    if (!req.body.name || 
        typeof req.body.employeesNumber !== "number" || req.body.employeesNumber < 0 ||
        typeof req.body.inceptionDate !== "number" || req.body.inceptionDate < 0 || // Date in UNIX time milliseonds
        typeof req.body.isPublic !== "boolean" ) {
        res.status(400).json({"error": "invalid input"});
        return;
    }

    try {
        const newOrganization = await Organization.addOrganization(req.body);
        res.status(201).json(newOrganization);
    } catch(err) {
        console.error(`v1AddOrganization: failed to add organization: ${err}`);
        res.status(500).json({ error: "failed to add organization" });
    }
};
