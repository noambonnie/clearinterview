const mongoose = require("mongoose");

const organizationSchema = new mongoose.Schema({ 

    // NOTE: We could add "createdAt" date that would allow for consistent sorting
    //       when paginating (to avoid a case where a record is added while)
    //       paginating. Leaving it out of the scope of this assignment but wanted to point it out
    name: String,
    inceptionDate: Date,
    employeesNumber: Number,
    isPublic: Boolean,
});

const organization = mongoose.model("organization", organizationSchema);

module.exports = organization;