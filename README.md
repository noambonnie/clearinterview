# Organizations Assignment - Noam Bonnie
I organized my code such that there's the DB layer (model), the API layer and the logic layer. This allows for separation of presentation from data.

# Database
I used MongoDB for my database. I did not assume any uniqueness requirements.

## Schema (`db/organization.js`)
```
    _id: ObjectID
    name: String,
    inceptionDate: Date,
    employeesNumber: Number,
    isPublic: Boolean,
```

_NOTE_: I'm currently using the MongoDB ID in responses. A real life system may want to have its own ID rather than rely on the DB's default IDs. This will allow for more control and easier migration to a different DB.

_NOTE_: We could add "createdAt" date that would allow for consistent sorting when paginating (to prevent a case where a record is added while paginating). I left it out of the scope of this assignment but wanted to point it out.

# Installation
```
npm install
```
Open `config/default.json` and edit the listen port and mongodb connection string.

Start the server:
```
npm start
```


# API
Invalid input returns `HTTP 400` (Bad Request).

## Create (`POST /v1/organization`)
Request JSON body:
```
{
    "name": <String>,
    "isPublic": <Boolean>,
    "employeesNumber": <Number>,
    "inceptionDate": <Number>
}
```
`inceptionDate` is specified in UNIX time in millisecods (e.g. 1600180942231)

Response returns the newly created organization in JSON format:
```
{
    "id": "5f60d33080e47e00175354a4",
    "name": "Success Test",
    "employeesNumber": 20,
    "isPublic": true,
    "inceptionDate": 1570097869123
}
```
Successful response returns `HTTP 201` (Created).

## Search (`GET /v1/organization/search`)
Query parameters are all optional. Results are returned for organizations that match _all_ criteria. I did not add a sorting option though that can easily be added.

`name` - a regular expression string (no need for '`/`')

`inceptionFrom` - minumum inception date (UNIX timestamp in milliseconds)

`inceptionTo` - maximum inception date (UNIX timestamp in milliseconds)

`employeesFrom` - minimum number of employees

`employeesTo` - maximum number of employees

`isPublic` - `"true"`/`"false"`

`limit` - limit the number of results. Default: 100. (a safeguard caps the limit at 1000).

`offset` - number of records to skip. For pagination.

Response returns a JSON object containing an array of organizations in the `results` field:
```
{
    "results": [{org1}, {org2}...]
}
```
Empty results will return an empty array in the `results` field.

Successful response returns `HTTP 200` (Success).
## Get (`GET /v1/organization/<org-id>`)
(This was not part of the assignment but I wrote it so might as well document it... )

Response body is a JSON object of the organization:
```
{
    "id": "5f60d33080e47e00175354a4",
    "name": "Success Test",
    "employeesNumber": 20,
    "isPublic": true,
    "inceptionDate": 1570097869123
}
```
`HTTP 200` (Success) is returned for successful response.
`HTTP 204` (No content) is returned if no organization with the provided ID exists.

# Testing
I used `newman` (a Postman tool) for testing. The tests are not complete, but they give a sense of how I would handle testing.

In the root directory
```
npx newman run -e tests/clear-heroku.postman_environment.json tests/Clear.postman_collection.json
```
_NOTE_: The test files point to the Heroku deployment. To change the host, edit the following file:
```
tests/clear-heroku.postman_environment.json
```

# Heroku deployment
The project is deployed in the following URL: [https://noam-clear-interview.herokuapp.com](https://noam-clear-interview.herokuapp.com)
